package controllers;

import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import static main.AppHelper.*;
import static main.FiltersHelper.*;


@SuppressWarnings("restriction")
public class AppController {
	
	@FXML
	ImageView image;
	
	@FXML
	void initialize () {
		
		
		Image img = new Image(getClass().getResourceAsStream("/images/portrait.png"), 500, 500, false, false);
//		image.setImage(medianFilter2(img));
		image.setImage(img);

		getPixels ();
	}
	
	@FXML
	private void applyFilter () {
		image.setImage(medianFilterGPU(image.getImage()));
	}
	
	private void getPixels () {
		
		PixelReader pixelReader = image.getImage().getPixelReader();
		
		for (int x = 0; x < image.getImage().getHeight(); x++) {
            for (int y = 0; y < image.getImage().getWidth(); y++) {
                Color color = pixelReader.getColor(x, y);
                int pixel = pixelReader.getArgb(x, y);
            }
        }
	}
}
