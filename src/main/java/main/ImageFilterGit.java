package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ImageFilterGit extends Application {
	public static void main(String [] args) {
    	launch(args);
//    	AppHelper.primeOnGPU();
	}

	@Override
	public void start(Stage stage) throws Exception {
		try {
            stage.setTitle("Image Filter");
            VBox page = FXMLLoader.load(getClass().getResource("/views/app.fxml"));
            Scene scene = new Scene(page);

            // stage.initStyle(StageStyle.UNDECORATED);
//            stage.initStyle(StageStyle.UNDECORATED);
//            scene.getStylesheets().add("/app_css/main.css");

//    		stage.setX(windows.getMinX());
//    		stage.setY(windows.getMinY());
//    		stage.setWidth(windows.getWidth());
//    		stage.setHeight(windows.getHeight());
    		
//    		stage.getIcons().add(new Image("/images/crypto_live.png"));

            stage.setScene(scene);
            stage.show();


        } catch (Exception ex) {
            System.out.println("Error initializing application!");
            ex.printStackTrace();

        }		
	}
}
