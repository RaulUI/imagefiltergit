package main;

import java.util.Arrays;

import com.aparapi.Kernel;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class FiltersHelper {

	public static Image toGrayscale (Image img) {
        long startTime = System.currentTimeMillis();
		PixelReader pixelReader = img.getPixelReader();
		int width = (int) img.getWidth();
		int height = (int) img.getHeight();
		
	    WritableImage grayImage = new WritableImage(width, height);
	    PixelWriter pixelWriter = grayImage.getPixelWriter(); 
		
		for (int x = 0; x < height; x++) {
            for (int y = 0; y < width; y++) {
                int pixel = pixelReader.getArgb(x, y);
                
                int red = ((pixel >> 16) & 0xff);
                int green = ((pixel >> 8) & 0xff);
                int blue = (pixel & 0xff);

                int grayLevel = (int) (0.2162 * (double)red + 0.7152 * (double)green + 0.0722 * (double)blue) / 3;
                grayLevel = 255 - grayLevel; // Inverted the grayLevel value here.
                int gray = (grayLevel << 16) + (grayLevel << 8) + grayLevel;

                pixelWriter.setArgb(x, y, -gray);
            }
        }

        System.out.printf("time taken without GPU: %s ms%n", System.currentTimeMillis() - startTime);
		return grayImage;
	}
	
	@SuppressWarnings("restriction")
	public static Image medianFilter (Image img) {
		
		long startTime = System.currentTimeMillis();
		PixelReader pixelReader = img.getPixelReader();
		int width = (int) img.getWidth();
		int height = (int) img.getHeight();
	    
		Color[] pixel=new Color[9];
        double[] R=new double[9];
        double[] B=new double[9];
        double[] G=new double[9];

	    WritableImage finalImg = new WritableImage(width, height);
	    PixelWriter pixelWriter = finalImg.getPixelWriter(); 

		for (int i = 1; i < height-1; i++) {
            for (int j = 1; j < width-1; j++) {
            	pixel[0]=pixelReader.getColor(i-1,j-1);
                pixel[1]=pixelReader.getColor(i-1,j);
                pixel[2]=pixelReader.getColor(i-1,j+1);
                pixel[3]=pixelReader.getColor(i,j+1);
                pixel[4]=pixelReader.getColor(i+1,j+1);
                pixel[5]=pixelReader.getColor(i+1,j);
                pixel[6]=pixelReader.getColor(i+1,j-1);
                pixel[7]=pixelReader.getColor(i,j-1);
                pixel[8]=pixelReader.getColor(i,j);
                for(int k=0;k<9;k++){
                    R[k]= pixel[k].getRed();
                    B[k]= pixel[k].getBlue();
                    G[k]= pixel[k].getGreen();
                }
                Arrays.sort(R);
                Arrays.sort(G);
                Arrays.sort(B);
                pixelWriter.setColor(i,j, new Color(R[4],G[4],B[4], 1));
            }
		}

        System.out.printf("time taken without GPU: %s ms%n", System.currentTimeMillis() - startTime);
	    
	    return finalImg;
	}
	
	
	@SuppressWarnings("restriction")
	public static Image medianFilterGPU (Image img) {
		
		long startTime = System.currentTimeMillis();
		PixelReader pixelReader = img.getPixelReader();
		int width = (int) img.getWidth();
		int height = (int) img.getHeight();
	    
		int[] pixel=new int[9];
        
        int[][] pixels = new int[width][height];

	    WritableImage finalImg = new WritableImage(width, height);
	    PixelWriter pixelWriter = finalImg.getPixelWriter(); 
		for (int i = 1; i < height-1; i++) {
            for (int j = 1; j < width-1; j++) {
            	pixels[i][j] = pixelReader.getArgb(i, j);
            }
		}
	    
	    Kernel kernel = new Kernel() {
            @Override
            public void run() {
    			for (int i = 1; i < height-1; i++) {
                    for (int j = 1; j < width-1; j++) {
                        int sum = 0;
                    	pixel[0]=pixels[i-1][j-1];
                        pixel[1]=pixels[i-1][j];
                        pixel[2]=pixels[i-1][j+1];
                        pixel[3]=pixels[i][j+1];
                        pixel[4]=pixels[i+1][j+1];
                        pixel[5]=pixels[i+1][j];
                        pixel[6]=pixels[i+1][j-1];
                        pixel[7]=pixels[i][j-1];
                        pixel[8]=pixels[i-1][j-1];
                        for (int k=0; k<9; k++) {
                        	sum+=pixel[k];
                        }
                        pixels[i][j] = sum / pixel.length;
                    }
        		}
            }
        };
        
		for (int i = 1; i < height-1; i++) {
            for (int j = 1; j < width-1; j++) {
            	pixelWriter.setArgb(i, j, pixels[i][j]);
            }
		}
        
        int size = height*width;
        
        kernel.execute(size);
        System.out.printf("time taken with GPU: %s ms%n", System.currentTimeMillis() - startTime);
        kernel.dispose();

	    return finalImg;
	}
}
