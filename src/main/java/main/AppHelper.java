package main;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

import com.aparapi.Kernel;
import java.util.Arrays;
import java.util.stream.IntStream;

public class AppHelper {

	public static Image toGrayscaleGPU (Image img) {
        long startTime = System.currentTimeMillis();
		PixelReader pixelReader = img.getPixelReader();
		int width = (int) img.getWidth();
		int height = (int) img.getHeight();
		
	    WritableImage grayImage = new WritableImage(width, height);
	    PixelWriter pixelWriter = grayImage.getPixelWriter(); 

        Kernel kernel = new Kernel() {
            @Override
            public void run() {
        		for (int x = 0; x < height; x++) {
                    for (int y = 0; y < width; y++) {
                        int pixel = pixelReader.getArgb(x, y);
                        
//                        int red = ((pixel >> 16) & 0xff);
//                        int green = ((pixel >> 8) & 0xff);
//                        int blue = (pixel & 0xff);

//                        int grayLevel = (int) (0.2162 * (double)red + 0.7152 * (double)green + 0.0722 * (double)blue) / 3;
//                        grayLevel = 255 - grayLevel; // Inverted the grayLevel value here.
//                        int gray = (grayLevel << 16) + (grayLevel << 8) + grayLevel;

//                        pixelWriter.setArgb(x, y, -gray);
                    }
                }
            }
        };
        
        int size = height*width;
        
        kernel.execute(size);
        System.out.printf("time taken with GPU: %s ms%n", System.currentTimeMillis() - startTime);
        kernel.dispose();
		return grayImage;
	}
	
	public static void primeOnGPU() {
		final int size = 100000;
        final int[] a = IntStream.range(2, size + 2).toArray();
        final boolean[] primeNumbers = new boolean[size];

        Kernel kernel = new Kernel() {
            @Override
            public void run() {
                int gid = getGlobalId();
                int num = a[gid];
                boolean prime = true;
                for (int i = 2; i < num; i++) {
                    if (num % i == 0) {
                        prime = false;
                        //break is not supported
                    }
                }
                primeNumbers[gid] = prime;
            }
        };
        long startTime = System.currentTimeMillis();
        kernel.execute(size);
        System.out.printf("time taken: %s ms%n", System.currentTimeMillis() - startTime);
        System.out.println(Arrays.toString(Arrays.copyOf(primeNumbers, 20)));//just print a sub array
        kernel.dispose();
	}
	
}
